<?php
/*
   Plugin Name: Tabs Creator   
   Description: Plugin that generates the HTML for tabs by providing the text for each tab as well as the page it links to. Example - [nd_tab link="/page-1/"]Page 1[/nd_tab] [nd_tab link="/page-2/"]Page 2[/nd_tab]
   Version: 1.0
   Author: ND   
   License: GPL2
*/

/* Example of shortcode usage:
[nd_tab link="/page-1/"]Page 1[/nd_tab]
[nd_tab link="/page-2/"]Page 2[/nd_tab]
*/

function nd_tabs_creator( $atts, $content ){
	
	// Get the URL that the user entered
	extract(shortcode_atts( array(
		'link' => '',
	), $atts ));
	
	global $post;
    $post_slug = '/'.$post->post_name; 			// Get the slug of the current page and put a slash at the front so it's similar to a link. i.e. page-1 becomes /page-1
	$users_slug = untrailingslashit( $link );	// Get the link the user entered and remove the trailing slash (if there is one). i.e. /page-1/ becomes /page-1
	
	// Compare the current page with the link the user entered. 
	// Give the tab for the current page a different css class so it can be styled differently to the page tabs. i.e. make it highlighted. 
	if ( strcasecmp( $post_slug, $users_slug ) == 0 ){		
		$class_name = 'active-tab';
		$selected = 'true';
	}
	else {
		$class_name = 'inactive-tab';
		$selected = 'false';
	}
	
	// Add extra CSS classes if you wanna
	$class_name = apply_filters( 'nd_tab_class', $class_name );
	
	$tab_string = '<div class="nd-tab left '. esc_attr( $class_name ) .'" aria-selected="'. esc_attr( $selected ) .'" role="tab"><a href="'. esc_url( $link ) .'">'. esc_html( $content ) .'</a></div>';
	
	return $tab_string;
}
add_shortcode( 'nd_tab', 'nd_tabs_creator' );

?>